<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class admin extends CI_Controller {
	public function __construct(){
		parent::__construct();

		$this->load->model('admin/admin_model');
		$this->load->model('admin/common_model');
	}
	
	public function check_user()	{
		$username = $this->input->post('username');
		$password = md5($this->input->post('password'));
		$this->load->model('admin/admin_model');
		$data=$this->admin_model->check_user($username,$password);
		if(!empty($data)){
			$admin_data=array(
				'id'=>$data['id'],	
				'name'=>$data['username'],	
			);
			$this->session->set_userdata($admin_data);
			echo 1;
		}else{
			echo 0 ;
		}
	}
	public function logout(){
		$this->session->sess_destroy();
		redirect(base_url()."admin");
	}
	
   
    function change_pass(){
		if ( !$this->session->userdata("id") ) {
			redirect(base_url()."admin");
			exit;
		}
		$params['title']='Change Password';
		$this->load->view('admin/includes/header',$params);
		$this->load->view('admin/login/change_password');
		$this->load->view('admin/includes/footer');
	}

	public function changepass()
	{
		if(!$this->session->userdata("id"))
		{
			redirect(base_url()."admin");
			exit;
		}
		$user_id= $this->session->userdata('id');
		$old_password = $this->input->post('old_password');
		$new_password = $this->input->post('new_password');
		$admin_name = $this->session->userdata('name');
		$admin_info = $this->admin_model->chngpass($admin_name,$old_password,$user_id);
		//debug($admin_info);die;
		if(empty($admin_info)){
			$this->session->set_userdata("success_message", "Old Password Does't Match"); 
			redirect( base_url() . "admin/admin/change_pass" );
		}
		else
		{
			$result=$this->admin_model->chng_pass($admin_name,$new_password);
			if($result)
			{
				$this->session->set_userdata("success_message","Password change successfully");
			}
			else
			{
				$this->session->set_userdata("success_message","Password can't change");
			}
			redirect(base_url()."admin/admin/change_pass");
		}


	}

	public function forgot_password()
	{
		$this->load->view('admin/login/forgot_password');
	}


  public function check_ajax_mail()	{
		$email = $this->input->post('email');
		$password = md5(rand(4,8));
		$this->load->model('admin/admin_model');
		$data=$this->admin_model->check_ajax_mail($email);
		if(!empty($data)){
			$data=array( 
			'password'=>$password 
                 );
			$result = $this->common_model->update('tbl_users',$data,'email',$email);
			if($result)
			{
				$result= array("status"=>"success","message" => 'Password send to your email successfully');
			}else{
				$result= array("status"=>"error","message" => 'Email not exist');
			}
		}else{
			$result= array("status"=>"error","message" => 'Error occured');
		}
		return json_encode($result) ;
	}
	
	}

