<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class users extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->table_user = 'tbl_users';
        $this->load->model('admin/common_model', 'c_m');
    }

    public function index() {
        if (!$this->session->userdata("id")) {
            redirect(base_url() . "admin");
            exit;
        }
        $params['title'] = 'List Users';
        $params['user_data'] = $this->c_m->get_data($this->table_user, $limit = 0, $limit_start = 0, $order_by = 'id', $order = 'DESC', 'is_active', '1');
        $this->load->view('admin/includes/header', $params);
        $this->load->view('admin/users/index');
        $this->load->view('admin/includes/footer');
    }

    public function edit_user($id) {
       $field = 'id';
        $value = urldecode(base64_decode($id));
        $params['title'] = 'Edit User';
        $params['user_data'] = $this->c_m->get_data_by_id($this->table_user, $field, $value);
   
        $this->load->view('admin/includes/header', $params);
        $this->load->view('admin/users/edit');
        $this->load->view('admin/includes/footer');
    }

}
