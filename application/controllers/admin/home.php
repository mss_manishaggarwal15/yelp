<?php
class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/common_model');
		if(!$this->session->userdata('id'))
		{
			redirect(base_url('admin'));
			exit;
		}
	}
	
	public function index() {
		if(!$this->session->userdata('id'))
		{
			redirect(base_url('admin'));
			exit;
		}
		$params['title']	=	'Home';
		$this->load->view('admin/includes/header',$params);
		
		$this->load->view('admin/login/home');
		$this->load->view('admin/includes/footer');
		
	}
	
	
}
