<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class faq extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->table_faq = 'tbl_faq';
        $this->load->library('form_validation');
        $this->form_validation->set_message('question', 'This is required.');
        $this->form_validation->set_message('answer', 'This is required.');

        $this->load->model('faq/faq_model');
        $this->load->model('admin/common_model', 'c_m');
    }

    public function index() {

        if (!$this->session->userdata("id")) {
            redirect(base_url() . "admin");
            exit;
        }
        $params['title'] = 'FAQ';
        $params['faq_data'] = $this->c_m->get_data($this->table_faq, $limit = 0, $limit_start = 0, $order_by = 'id', $order = 'DESC', 'is_deleted', '0');

        $this->load->view('admin/includes/header', $params);
        $this->load->view('admin/faq/index');
        $this->load->view('admin/includes/footer');
    }

    public function add_faq($param) {
        if (!$this->session->userdata("id")) {
            redirect(base_url() . "admin");
            exit;
        }
        $params['title'] = 'Add FAQ';

        $this->load->view('admin/includes/header', $params);
        $this->load->view('admin/faq/create');
        $this->load->view('admin/includes/footer');
    }

    public function create_faq($param) {
        if (!$this->session->userdata("id")) {
            redirect(base_url() . "admin");
            exit;
        }
        $question = $this->input->post('question');
        $answer = $this->input->post('answer');
//        $answer = strip_tags($answer);
        $faq_data = array('question' => $question, 'answer' => $answer);
        $this->c_m->insert_data($faq_data, $this->table_faq);
        redirect(base_url('admin/faq'));
        exit;
    }

    public function edit_faq($id) {
        if (!$this->session->userdata("id")) {
            redirect(base_url() . "admin");
            exit;
        }
        $field = 'id';
        $value = urldecode(base64_decode($id));
        $params['title'] = 'Edit FAQ';
        $params['faq_data'] = $this->c_m->get_data_by_id($this->table_faq, $field, $value);
        //debug($params['faq_data']);
//         if (isset($_POST('update_faq'))) {
//         print_r($this->input->post('update_faq'));
//            $question = $this->input->post('question');
//            $answer = $this->input->post('answer');
// //        $answer = strip_tags($answer);
//            $faq_data = array('question' => $question, 'answer' => $answer);
//            $this->c_m->update_faq($table_faq, $field, $faq_data);
//         }
//
        $this->load->view('admin/includes/header', $params);
        $this->load->view('admin/faq/edit');
        $this->load->view('admin/includes/footer');
//        
    }

    public function update_faq() {
        $id = $this->input->post('id');
        //  debug($id);
        if (!$this->session->userdata("id")) {
            redirect(base_url() . "admin");
            exit;
        }
        $data = array(
            'question' => $this->input->post('question'),
            'answer' => $this->input->post('answer'),
        );
        $update = $this->c_m->update($this->table_faq, $data, 'id', $id);
        if ($update) {
            $this->session->set_userdata('success_message', 'Data Updated successfully.');
        } else {
            $this->session->set_userdata('success_message', 'Error Occured.');
        }
        redirect(base_url() . "admin/faq");
    }

    public function view_faq($id) {
        if (!$this->session->userdata("id")) {
            redirect(base_url() . "admin");
            exit;
        }
        $value = urldecode(base64_decode($id));
        $field = 'id';
        $data['faq_data'] = $this->c_m->get_data_by_id($this->table_faq, $field, $value);
        $data['title'] = 'View FAQ';
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/faq/view');
        $this->load->view('admin/includes/footer');
    }

    public function delete_faq($id) {
        if (!$this->session->userdata("id")) {
            redirect(base_url() . "admin");
            exit;
        }
        $value = base64_decode(urldecode($id));
        $field = 'id';
        $delete = $this->c_m->changeDeleteStatus($this->table_faq, $field, $value);
        if ($delete) {
            $this->session->set_userdata('success_message', 'Data Deleted Successfully.');
        } else {
            $this->session->set_userdata('success_message', 'ERROR OCCURED!');
        }
        redirect(base_url() . 'admin/faq');
    }

}
