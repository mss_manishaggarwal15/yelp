<script type="text/javascript">
    jQuery(document).ready(function () {
        FormValidation.init();
        
    });
   

</script>
<style>
    fieldset{color: #42382F !important;}
</style>
<div class="row-fluid">
    <!-- block -->
    <div class="block">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><?php echo $title; ?></div>
            <ul><li> <a href="<?php echo base_url();?>admin/faq" >
                    <div class="btn btn-info pull-right"  >Go Back</div>
                    </a>          </li></ul>
 
        </div>

            <fieldset>

                <div class="control-group">
                    <label class="control-label" for="question">Question </label>
                    <div class="controls">
                        <?php echo $faq_data['question']; ?>
                    </div>

                </div>
                <div class="control-group">
                    <label class="control-label" for="answer">Answer</label>
                    <div class="controls">
                        <?php echo $faq_data['answer']; ?>
                    </div>
                </div>
            
            </fieldset>
        </form>
    </div>
    <!-- /block -->
</div>
</div></div></div>