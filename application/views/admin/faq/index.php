<style>
    fieldset{color: #42382F !important;}
</style>
<div class="row-fluid">
    <!-- block -->
    <div class="block">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><?php echo $title; ?></div>
        </div>

    <div><b><?php if ( $this->session->userdata("success_message")) { ?>
<div class="alert alert-success">

  <?php echo $this->session->userdata("success_message");
   $this->session->unset_userdata("success_message"); } ?> 

   </div>  
        <div class="block-content collapse in">
            <div class="span12">
                <div class="table-toolbar">
                    <div class="btn-group">
                        <a href="<?php echo site_url('admin/add_faq'); ?>"><button class="btn btn-success">Add New <i class="icon-plus icon-white"></i></button></a>
                    </div>
                    <div class="btn-group pull-right">
                        <button data-toggle="dropdown" class="btn dropdown-toggle">Tools <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#">Print</a></li>
                            <li><a href="#">Save as PDF</a></li>
                            <li><a href="#">Export to Excel</a></li>
                        </ul>
                    </div>
                </div>

                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example2">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Title</th>

                            <!--<th>Tags</th>-->
                            <th>Action</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                      
                        foreach ($faq_data as $data) : ?>
                        <tr class="odd gradeX">
                            <td><input type="checkbox"></td>
                            <td><?php echo $data['question']; ?></td>
                            <!--<td><?php // echo $data['question']; ?></td>-->
                            <td><a href="<?php echo site_url('admin/edit_faq/'.  urlencode(base64_encode($data['id'])));?>">Edit</a> |
                            <a href="<?php echo site_url('admin/delete_faq/'.  urlencode(base64_encode($data['id'])));?>">Delete</a> |
                            <a href="<?php echo site_url('admin/view_faq/'.  urlencode(base64_encode($data['id'])));?>">View</a></td>


                        </tr>
                        <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- /block -->
</div>
</div></div></div>
