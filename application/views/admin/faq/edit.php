<script type="text/javascript">
    jQuery(document).ready(function () {
        FormValidation.init();
        
    });
   

</script>
<style>
    fieldset{color: #42382F !important;}
</style>
<div class="row-fluid">
    <!-- block -->
    <div class="block">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><?php echo $title; ?></div>
                <li> <a href="<?php echo base_url();?>admin/faq" >
                    <div class="btn btn-info pull-right"  >Go Back</div>
                </a>          </li>
 
        </div>

        <form class="form-horizontal" action="<?php echo site_url('admin/update_faq');?>" method="POST">
            <fieldset>

                <div class="control-group">
                    <label class="control-label" for="question">Question </label>
                    <div class="controls">
                        <input type="text" class="span6" name="question" required="required" value="<?php echo $faq_data['question']; ?>">
                    </div>
                <input type="hidden" name="id" id="id" value="<?php if(!empty($faq_data['id'])){ echo $faq_data['id']; }?>">

                </div>
                <div class="control-group">
                    <label class="control-label" for="answer">Answer</label>
                    <div class="controls">
                        <textarea class="input-xlarge textarea" name="answer" required="required" placeholder="Enter answer ..." style="width: 810px; height: 200px" ><?php echo $faq_data['answer']; ?></textarea>
                    </div>
                </div>
                <div class="form-actions">
                    <input type="submit"  name="update_faq" class="btn btn-primary" value="Update">
                    <button type="reset" class="btn">Cancel</button>
                </div>
            </fieldset>
        </form>
    </div>
    <!-- /block -->
</div>
</div></div></div>