<script type="text/javascript">
    jQuery(document).ready(function () {
        FormValidation.init();

    });


</script>
<style>
    fieldset{color: #42382F !important;}
</style>
<div class="row-fluid">
    <!-- block -->
    <div class="block">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><?php echo $title; ?></div>
            <li> <a href="<?php echo base_url(); ?>admin/users" >
                    <div class="btn btn-info pull-right"  >Go Back</div>
                </a>          </li>

        </div>

        <form class="form-horizontal" action="<?php echo site_url('admin/update_users'); ?>" method="POST">
            <fieldset>

                <div class="control-group">
                    <label class="control-label" for="first_name">First Name </label>
                    <div class="controls">
                        <input type="text" class="span6" name="first_name" required="required" value="<?php echo $user_data['first_name']; ?>">
                    </div>
                    <input type="hidden" name="id" id="id" value="<?php
                    if (!empty($user_data['id'])) {
                        echo $user_data['id'];
                    }
                    ?>">

                </div>
                <div class="control-group">
                    <label class="control-label" for="answer">Last Name </label>
                    <div class="controls">
                        <input type="text" class="span6" name="last_name" required="required" value="<?php echo $user_data['last_name']; ?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="answer">Role </label>
                    <div class="controls">

                        <select name="role">
                            <option value="">-Select-</option>
                            <option value="admin" <?php if ($user_data['role'] == 'admin') {
                        echo 'selected=selected';
                    } ?>>Admin</option>
                            <option value="business_user" <?php if ($user_data['role'] == 'business_user') {
                        echo 'selected=selected';
                    } ?>>Business User</option>
                            <option value="user" <?php if ($user_data['role'] == 'user') {
                        echo 'selected=selected';
                    } ?>>User</option>
                        </select>                    </div>
                </div>
                <div class="form-actions">
                    <input type="submit"  name="update_faq" class="btn btn-primary" value="Update">
                    <button type="reset" class="btn">Cancel</button>
                </div>
            </fieldset>
        </form>
    </div>
    <!-- /block -->
</div>
</div></div></div>