<script type="text/javascript">
    jQuery(document).ready(function () {
        FormValidation.init();
    });
   

</script>
<style>
    fieldset{color: #42382F !important;}
</style>
<div class="row-fluid">
    <!-- block -->
    <div class="block">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><?php echo $title; ?></div>
        </div>

        <form class="form-horizontal" action="<?php echo site_url('admin/faq_create');?>" method="POST">
            <fieldset>

                <div class="control-group">
                    <label class="control-label" for="question">Question </label>
                    <div class="controls">
                        <input type="text" class="span6" name="question" required="required">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="answer">Answer</label>
                    <div class="controls">
                        <textarea class="input-xlarge textarea" name="answer" required="required" placeholder="Enter answer ..." style="width: 810px; height: 200px"></textarea>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="submit" name="save_faq" class="btn btn-primary">Save</button>
                    <button type="reset" class="btn">Cancel</button>
                </div>
            </fieldset>
        </form>
    </div>
    <!-- /block -->
</div>
</div></div></div>