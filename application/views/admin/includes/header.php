<!DOCTYPE html>
<html class="no-js">
    <head>
        <title>Admin Panel</title>
        <link rel="shortcut icon" href="<?php echo base_url(); ?>images/favicon.ico" type="image/x-icon"/>
        <!-- Bootstrap -->
        <link href="<?php echo base_url(); ?>webroot/css/admin/style.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>webroot/css/admin/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="<?php echo base_url(); ?>webroot/css/admin/bootstrap-responsive.min.css" rel="stylesheet" >
        <link href="<?php echo base_url(); ?>webroot/css/admin/jquery.easy-pie-chart.css" rel="stylesheet" media="screen">
        <link href="<?php echo base_url(); ?>webroot/css/admin/styles.css" rel="stylesheet" media="screen">
        <link rel="stylesheet" href="<?php echo base_url(); ?>webroot/css/admin/validationEngine.jquery.css"> 
        <link rel="stylesheet" href="<?php echo base_url(); ?>webroot/css/admin/jquery-ui.css"> 
        <link rel="stylesheet" href="<?php echo base_url(); ?>webroot/css/admin/responsive.css"> 
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
                    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
                <![endif]-->
        <script type="text/javascript">
            var base_url = '<?php echo base_url(); ?>';
            var uri_seg = '<?php echo $this->uri->segment(3, 0); ?>';
        </script>
        <!--<script type="text/javascript" src="<?php echo base_url(); ?>webroot/js/admin/jquery-1.9.1.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>webroot/js/jquery.validate.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>webroot/js/admin/jquery.validationEngine.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>webroot/js/admin/jquery.validationEngine-en.js"></script>-->
        <script type="text/javascript" src="<?php echo base_url(); ?>webroot/js/admin/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>webroot/js/admin/jquery.validate.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>webroot/js/admin/jquery.validate.min.js"></script>
        <!--<script type="text/javascript" src="<?php echo base_url(); ?>webroot/js/admin/jquery-ui.js"></script>-->
        <script type="text/javascript" src="<?php echo base_url(); ?>webroot/js/admin/active.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>webroot/js/form-validation.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>webroot/js/scripts.js"></script>
        <script src="<?php echo base_url(); ?>webroot/js/wysiwyg/wysihtml5-0.3.0.js"></script>
        <script src="<?php echo base_url(); ?>webroot/js/wysiwyg/bootstrap-wysihtml5.js"></script>
                <link href="<?php echo base_url(); ?>webroot/js/wysiwyg/bootstrap-wysihtml5.css" rel="stylesheet" media="screen">

        <script>
            $(document).ready(function () {
                $('#clickleft').click(function () {

                    $('.bs-docs-sidenav').fadeToggle();

                });
            });
            $(function () {
                $('.textarea').wysihtml5();
            });
        </script>
    </head>
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner headerr">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="<?php echo base_url() . "admin/home"; ?>" style="color:#fff !important; ">Admin Panel</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown admin">
                                <a href="#" role="button" class="dropdown-toggle sideadmin" data-toggle="dropdown"><i class="icon-user"></i><?php echo ucfirst($this->
        session->userdata('name'));
?><i class="caret"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="<?php echo base_url(); ?>admin/admin/change_pass">Change Password</a>
                                    </li>


                                    <li class="divider"></li>
                                    <li>
                                        <a tabindex="-1" href="<?php echo base_url(); ?>admin/admin/logout">Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>

                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span3" id="sidebar">
                    <div class="toggle" id="clickleft">
                        <img src="<?php echo base_url(); ?>webroot/images/admin/lines7.png">
                    </div>
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse sidelist">
                        <li>
                            <a href="<?php echo base_url(); ?>admin/home"><i class="icon-chevron-right"></i>Dashboard</a>
                        </li>
                        <?php
                        // echo $menu_active;
                        ?>
                        <li>
                        <a href="<?php echo base_url(); ?>admin/users"><i class="icon-chevron-right"></i>Manage Users</a>
                        </li>
                        	
                        <li <?php if (!empty($menu_active) && $menu_active == "FAQ") {
                            echo "class='active'";
                        } ?>>
                            <a href="<?php echo base_url('admin/faq'); ?>"><i class="fa fa-question-circle-o" aria-hidden="true"></i>FAQ</a>
                        </li>	


                    </ul>
                </div>
                <!--/span-->
                <div class="span9" id="content">