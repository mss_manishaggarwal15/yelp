<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
<!-- disable iPhone inital scale -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Forgot Password :: Admin Panel</title>
<link rel="shortcut icon" href="<?php echo base_url();?>images/favicon.ico" type="image/x-icon"/>
<link href="<?php echo base_url();?>webroot/css/admin/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>webroot/css/media-queries.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="<?php echo base_url();?>images/favicon.ico" type="image/x-icon" /> 
<!-- html5.js for IE less than 9 -->
<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!-- css3-mediaqueries.js for IE less than 9 -->
<!--[if lt IE 9]>
	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->
<!--[if lt IE 9]><script src="scripts/html5shiv.js"></script><![endif]-->
<script type="text/javascript" src="<?php echo base_url(); ?>webroot/js/admin/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>webroot/js/admin/general.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>webroot/js/admin/jquery.validate.js"></script>
</head>
<script type="text/javascript">
$(document).ready(function(){
  forgot_password();
})  
</script>
<script>
 base_url='<?php echo base_url(); ?>';
 </script>
<style>

.btn-primary{
color:#fff;text-shadow:0 -1px 0 rgba(0,0,0,0.25);background-color:#1C1713;*background-color:#04c;background-image:-moz-linear-gradient(top,#1C1713,#1C1713);background-image:-webkit-gradient(linear,0 0,0 100%,from(#1C1713),to(#1C1713));background-image:-webkit-linear-gradient(top,#1C1713,#1C1713);background-image:-o-linear-gradient(top,#1C1713,#1C1713);background-image:linear-gradient(to bottom,#1C1713,#1C1713);background-repeat:repeat-x;border-color:#04c #04c #002a80;border-color:rgba(0,0,0,0.1) rgba(0,0,0,0.1) rgba(0,0,0,0.25);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff0088cc',endColorstr='#ff0044cc',GradientType=0);filter:progid:DXImageTransform.Microsoft.gradient(enabled=false)
}
 label.error
{
color:red;
}
</style>
<body>
<div class="header">

<div class="body_main">
	<div id="body_wrapper">
		<div class="featured_phone_bg">
			<form class="form-signin" id="forgot_password">
				<h2 class="form-signin-heading">Forgot Password</h2>
				<input type="text" name="email" id="email"placeholder="Email">
				<button class="btn-primary btn" type="submit">Send Link</button>
			</form>
		</div>
	</div>
</div>
</body>
</html>