<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
<!-- disable iPhone inital scale -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Codeigniter :: Admin Panel</title>
<!-- html5.js for IE less than 9 -->
<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!-- css3-mediaqueries.js for IE less than 9 -->
<!--[if lt IE 9]>
	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->
<!--[if lt IE 9]><script src="scripts/html5shiv.js"></script><![endif]-->
<script type="text/javascript" src="<?php echo base_url(); ?>webroot/js/admin/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>webroot/js/admin/general.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>webroot/js/admin/jquery.validate.js"></script>
</head>
<script type="text/javascript">
$(document).ready(function(){
  validation();
})  
</script>
<script>
 base_url='<?php echo base_url(); ?>';
 </script>
<style>
 

 label.error
{
color:red;
}
</style>
<body>
<div class="fullbody">
    <div class="header">
        <div id="header_wrapper">
            <div class="logo">
                <a href="<?php echo base_url();?>admin"></a>
            </div>
        </div>
    </div>
<div class="body_main">
	<div id="body_wrapper">
		<div class="featured_phone_bg">
			<form class="form-signin" id="login_button"> 

				<h2 class="form-signin-heading" style="text-align: center; font-family:Gotham, 'Helvetica Neue', Helvetica, Arial, sans-serif; color:#1CC0CD ">Please sign in</h2>
				<input type="text" name="username" id="username" class="input-block-level" placeholder="Username">
				<input type="password" name="password" id="password" class="input-block-level" placeholder="Password"/></br>
				
			<a tabindex="-1" href="<?php echo base_url(); ?>admin/admin/forgot_password">Forgot Password</a>
				<button class="btn-primary btn" type="submit" style="margin-top:20px;">Sign in</button>
			</form>
		</div>
	</div>
</div>
</div>
</body>
</html>