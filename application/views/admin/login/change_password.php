<script type="text/javascript">
<!--	$(document).ready(function() {	
    jQuery("#change_password").validationEngine();
    }); //-->
				
</script>
<style>
    fieldset{color: #42382F !important;}
</style>
<div class="row-fluid">
    <!-- block -->	<div class="block">	
        <div class="navbar navbar-inner block-header">	
            <div class="muted pull-left">Change Password</div>
            <a href="<?php echo base_url() ?>admin/home" >
                <div class="muted pull-right">Go Back</div>
            </a>	</div>	
        <div class="block-content collapse in">	
            <div class="span3" style="width:100%;color:#114069">
                <div id="message_container" style="margin-left:200px;color:red;">			<?php
                    if ($this->session->userdata("success_message")) {
                        echo $this->session->userdata("success_message");
                        $this->session->unset_userdata("success_message");
                    }
                    ?>
                </div>	
                <form style="margin-top:45px;" action="<?php echo base_url(); ?>admin/admin/changepass" class="form-horizontal" id="change_password" name="change_password" method="post">
                    <fieldset>			
                        <div class="control-group">	
                            <label class="control-label" for="focusedInput">Old Password</label>	<div class="controls">	
                                <input class="input-xlarge text-input" name="old_password" id="old_password" type="password" value="" placeholder="Old Password">
                            </div>	
                        </div>		
                        <div class="control-group">	
                            <label class="control-label" for="focusedInput">New Password</label>
                            <div class="controls">	
                                <input class="input-xlarge text-input" name="new_password" id="new_password" type="password" value="" placeholder="New Password">		</div>	
                        </div>	
                        <div class="control-group">	
                            <label class="control-label" for="focusedInput">Confirm Password</label>	
                            <div class="controls">
                                <input class="input-xlarge" name="confirm_password" id="confirm_password" type="password" value="" placeholder="Confrm Password">
                            </div>	
                        </div>	
                        <div class="control-group">	
                            <div class="controls">
                                <input type="image" id="chngpass" src="<?php echo base_url() . "webroot/images/admin/change_password.png" ?>" style="width:150px" alt="change password">	
                            </div>	
                        </div>	
                    </fieldset>	
                </form>		
            </div>
        </div>	
    </div>	
    <!-- /block -->
</div>
</div></div></div>
<script>
$("#change_password").validate({
rules: 	{
"old_password": "required",
"new_password": "required",
"confirm_password": {
required: true,
equalTo: "#new_password"
},
},
messages: 	{
"old_password": { required:"Enter your current password"},
"new_password": { required:"Enter your new password"},
"confirm_password": { required:"Confirm new password"},
equalTo: "Password must be equal"	}});
</script>