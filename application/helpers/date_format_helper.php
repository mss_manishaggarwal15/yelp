<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
function format_date($date) {
    $parts = explode('-', $date);
    return date('F j, Y', mktime(0, 0, 0, $parts[1], $parts[2], $parts[0]));
}

?>